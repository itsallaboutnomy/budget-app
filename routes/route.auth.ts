import express, {  Router  } from "express";
import AuthController from "../controllers/controller.auth";
import { checkJwt } from "../middlewares/checkJwt";

const router = Router();
//Login route
// router.get("/login", (req :Request, res :Response) =>{
router.get("/login", (req, res) =>{
    res.status(200).send('Login');
});
router.post("/login", AuthController.login);

//Change my password
router.post("/change-password", [checkJwt], AuthController.changePassword);

export default router;
