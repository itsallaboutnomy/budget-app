const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const db = require('../models/index');
const users = require('../controllers/controller.users');
const checkJwt = require("../middlewares/checkJwt");
//  console.log(db.User.findAll());

/**
 * @swagger
 * components:
 *   schemas:
 *      User:
 *       type: object
 *       required:
 *         - first_name
 *         - email
 *         - password
 *       properties:
 *         first_name:
 *           type: string
 *           description: User First Name
 *         last_name:
 *           type: string
 *           description: User Last Name
 *         password:
 *           type: string
 *           description: User Last Name
 *         email:
 *           type: email
 *           description: The user email
 *         phone_number:
 *           type: string
 *           description: The user phone number
 *         preferences:
 *           type: json
 *           description: The user preferences
 *       example:
 *         first_name: Muhammad Nauman
 *         last_name: Yousaf
 *         email: nauman.yousaf@invozone.com
 *         password: 123456
 *         phone_number: 00923218840145
 *         preferences: {"start_balance": "Muhammad Nauman","end_balance": "nauman.yousaf@invozone.com"}
 *          
 */

 /**
  * @swagger
  * tags:
  *   name: Users
  *   description: The users managing API
  */

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Returns the list of all the users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: The list of the users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */

router.get("/", checkJwt, users.getAll);
// router.get("/", (req,res ) => {
//   db.Users.findAll().then((data) => {
//       if(!data){
//           res.sendStatus(404)
//       }
//       res.send(data);
//   });
// });

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     responses:
 *       200:
 *         description: The user description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: The user was not found
 */

router.get("/:id", users.getRec);

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a new user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */

router.post("/", users.createRec);

/**
 * @swagger
 * /users/{id}:
 *  put:
 *    summary: Update the user by the id
 *    tags: [Users]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The user id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: The user was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      404:
 *        description: The user was not found
 *      500:
 *        description: Some error happened
 */

router.put("/:id", users.updateRec);

/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: Remove the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 * 
 *     responses:
 *       200:
 *         description: The user was deleted
 *       404:
 *         description: The user was not found
 */

router.delete("/:id", users.deleteRec);

module.exports = router;
