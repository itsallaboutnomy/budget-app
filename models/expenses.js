'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Expenses extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Expenses.init({
    description: DataTypes.TEXT,
    amount: DataTypes.FLOAT,
    is_shared: DataTypes.BOOLEAN,
    category_id: DataTypes.INTEGER,
    model_type: DataTypes.ENUM(['individual', 'group', 'friend']),
    model_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    budget_id: DataTypes.INTEGER,
    division: DataTypes.JSON,
    is_settled: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE

  }, {
    sequelize,
    modelName: 'Expenses',
    tableName: 'expenses',
  });
  return Expenses;
};
